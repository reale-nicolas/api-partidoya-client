<?php
namespace RealeNicolas\ApiPartidoyaClient\Contracts;

interface ClientInterface {

    /**
     * Undocumented function
     *
     * @param array $data
     * 
     * @return mixed
     */
    public function create(array $data);


    /**
     * Undocumented function
     *
     * @param integer $id
     * @param array $data
     * 
     * @return mixed
     */
    public function update(int $id, array $data);

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return mixed
     */
    public function delete(int $id);

  

    /**
     * Undocumented function
     *
     * @param int $id
     * 
     * @return mixed
     */
    public function getById(int $id);

    /**
     * Undocumented function
     *
     * @param string $name
     * 
     * @return mixed
     */
    public function getByName(string $name);


    /**
     * Undocumented function
     *
     * @param array $data
     * 
     * @return mixed
     */
    public function getBy(array $data);

  
}