<?php
namespace RealeNicolas\ApiPartidoyaClient;

use RealeNicolas\ApiPartidoyaClient\Contracts\ClubAddressInterface;

/**
 * 
 */
Class ClubAddressClient extends ApiPartidoYaClient implements ClubAddressInterface {


    //Address
    public const URI_CLUB_GET_BY_ADDRESS            = 'api/club/find/address';
    public const URI_CLUB_UPDATE_ADDRESS            = 'api/club/update/address';


    /**
     * Undocumented function
     */
    public function __construct(array $conf) {
        parent::__construct($conf);
    }


    /**
     * Undocumented function
     *
     * @return string
     */
    public function getCreateEndpoint() {
        return '';
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getUpdateEndpoint(int $id) {
        return sprintf(self::URI_CLUB_UPDATE_ADDRESS."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getDeleteEndpoint(int $id) {
        return '';
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getGetByIdEndpoint(int $id) {
        return '';
    }

    /**
     * Undocumented function
     *
     * @param string $name
     * 
     * @return string
     */
    public function getGetByNameEndpoint(string $name) {
        return '';
    }

    /**
     * Undocumented function
     * 
     * @return string
     */
    public function getGetByEndpoint() {
        return (self::URI_CLUB_GET_BY_ADDRESS);
    }
}
