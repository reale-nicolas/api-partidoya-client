<?php
namespace RealeNicolas\ApiPartidoyaClient;

use RealeNicolas\ApiPartidoyaClient\Contracts\ClubClientInterface;

/**
 * 
 */
Class ClubClient extends ApiPartidoYaClient implements ClubClientInterface{

    //CRUD Club
    public const URI_CLUB_CREATE                    = 'api/club/create';
    public const URI_CLUB_UPDATE                    = 'api/club/update';
    public const URI_CLUB_DELETE                    = 'api/club/delete';
    public const URI_CLUB_GET_BY_ID                 = 'api/club/find/id';
    public const URI_CLUB_GET_BY                    = 'api/club/find';

    //Name
    public const URI_CLUB_GET_BY_NAME               = 'api/club/find/name';


    //Comments
    public const URI_CLUB_GET_COMMENT               = 'api/club/comments';
    public const URI_CLUB_CREATE_COMMENT            = 'api/club/comments';
    

    /**
     * Undocumented function
     */
    public function __construct(array $conf) {
        parent::__construct($conf);
    }


    /**
     * Undocumented function
     *
     * @return string
     */
    public function getCreateEndpoint() {
        return self::URI_CLUB_CREATE;
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getUpdateEndpoint(int $id) {
        return sprintf(self::URI_CLUB_UPDATE."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getDeleteEndpoint(int $id) {
        return sprintf(self::URI_CLUB_DELETE."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getGetByIdEndpoint(int $id) {
        return sprintf(self::URI_CLUB_GET_BY_ID."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param string $name
     * 
     * @return string
     */
    public function getGetByNameEndpoint(string $name) {
        return sprintf(self::URI_CLUB_GET_BY_NAME."/%s", $name);
    }

    /**
     * Undocumented function
     * 
     * @return string
     */
    public function getGetByEndpoint() {
        return (self::URI_CLUB_GET_BY);
    }
}
