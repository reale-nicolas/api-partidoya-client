<?php

namespace RealeNicolas\ApiPartidoyaClient;

use Exception;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use RealeNicolas\ApiPartidoyaClient\Contracts\ClientInterface;
use RealeNicolas\ApiPartidoyaClient\Exceptions\ValidationException;
use RealeNicolas\ApiPartidoyaClient\Exceptions\InvalidClientException;



abstract class ApiPartidoYaClient implements ClientInterface{

    private $client;
    protected $accessToken;

    protected $method;
    protected $uri;
    protected $data;
    protected $response;

    /**
     * Undocumented function
     *
     * @return string
     */
    public abstract function getCreateEndpoint();

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public abstract function getUpdateEndpoint(int $id);

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public abstract function getDeleteEndpoint(int $id);

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public abstract function getGetByIdEndpoint(int $id);

    /**
     * Undocumented function
     *
     * @param string $name
     * 
     * @return string
     */
    public abstract function getGetByNameEndpoint(string $name);

    /**
     * Undocumented function
     * 
     * @return string
     */
    public abstract function getGetByEndpoint();



    /**
     * Undocumented function
     */
    public function __construct(array $conf) 
    {
        $auth = $this->getAuthorization($conf);

        $headers = [
            // Base URI is used with relative requests
            'base_uri'  => 'http://api.partidoya.com:8080',
            'timeout'   => 20,
            'Accept'    => 'application/json',
            'Content-Type'  => 'application/json',
            'Authorization' => $auth,
        ];
        
        $this->client = new Client($headers);

       
    }


  
    protected function getAuthorization($conf)
    {
        
        if (!$this->accessToken) {
            $token = $this->getAccessToken($conf['client_id'], $conf['client_secret']);
            $this->accessToken = $token['access_token'];
        }

        return 'Bearer '.$this->accessToken;
        /*$headers = [
            'Authorization' => 'Bearer '.$this->accessToken,
        ];

        $data['headers'] = $headers;

        return $data;*/
        
    }

   
    private function getAccessToken($clientId, $clientSecret) 
    {
        $response = $this->client->request('POST', '/oauth/token', [
            'form_params'  => [
                'grant_type'    => 'client_credentials',
                'client_id'     => $clientId, 
                'client_secret' => $clientSecret,
                'scope'         => ''
            ],
            // 'auth' => [
            //     $connectionParamaters['client_id'], 
            //     $connectionParamaters['client_secret']
            // ]
        ]);

        $data = json_decode($response->getBody(), true);

        return $data;
    }
    /**
     * Undocumented function
     *
     * @param string $method
     * @param string $uri
     * @param array $data
     * 
     * @return mixed
     */
    protected function request(string $method, string $uri = '', array $data = [])
    {
        $this->setMethod($method);
        $this->setUri($uri);
        $this->setData($data);
        
        //$header = $this->getAuthorization();
        $response = $this->client->request($method, $uri, $data);

        return $this->proccessResponse($response);
    }

    /**
     * Undocumented function
     *
     * @param array $data
     * 
     * @return mixed
     */
    public function create(array $data)
    {
        $endpoint = $this->getCreateEndpoint();

        return $this->request('PUT', $endpoint, [
            'form_params'  => $data,
        ]);
    }



    /**
     * Undocumented function
     *
     * @param integer $id
     * @param array $data
     * 
     * @return mixed
     */
    public function update(int $id, $data)
    {
        $endpoint = $this->getUpdateEndpoint($id);

        return $this->request('PATCH', $endpoint, [
            'form_params'  => $data,
        ]);
    }



    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return mixed
     */
    public function delete(int $id)
    {
        $endpoint = $this->getDeleteEndpoint($id);

        return $this->request('DELETE', $endpoint);
    }


    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return mixed
     */
    public function getById(int $id)
    {
        $endpoint = $this->getGetByIdEndpoint($id);

        // Send a request to partidoYa api
        return $this->request('GET', $endpoint);
    }


    /**
     * Undocumented function
     *
     * @param string $name
     * 
     * @return mixed
     */
    public function getByName(string $name)
    {
        $endpoint = $this->getGetByNameEndpoint($name);

        // Send a request to partidoYa api
        return $this->request('GET', $endpoint);
    }


    /**
     * Undocumented function
     *
     * @param array $data
     * 
     * @return mixed
     */
    public function getBy(array $data)
    {
        $endpoint = $this->getGetByEndpoint();

        // Send a request to partidoYa api
        return $this->request('GET', $endpoint, [
            'form_params'  => $data,
        ]);
    }


    /**
     * Undocumented function
     *
     * @param ResponseInterface $response
     * @return mixed
     */
    protected function proccessResponse(ResponseInterface $response) {

        $this->setResponse($response);
        switch ($response->getStatusCode()) {

            case 200:
            case 201:
                return json_decode($response->getBody());
            break;

            case 403:
                $data = json_decode($response->getBody(), true);
                throw new InvalidClientException();
            break;

            case 422:
                $data = json_decode($response->getBody(), true);
                throw new ValidationException($data['status'], $data['errors']);
            break;
                
            default:
                $data = json_decode($response->getBody(), true);
                throw new Exception($data, $response->getStatusCode());
        }
    }



    
    /**
     * Get the value of method
     */ 
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set the value of method
     *
     * @return  self
     */ 
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get the value of uri
     */ 
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set the value of uri
     *
     * @return  self
     */ 
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get the value of data
     */ 
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @return  self
     */ 
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get the value of response
     */ 
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set the value of response
     *
     * @return  self
     */ 
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }
}