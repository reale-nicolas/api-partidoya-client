<?php
namespace RealeNicolas\ApiPartidoyaClient;

use RealeNicolas\ApiPartidoyaClient\Contracts\LocalityClientInterface;

/**
 * 
 */
Class LocalityClient extends ApiPartidoYaClient implements LocalityClientInterface{

   //CRUD Locality
   public const URI_LOCALITY_CREATE                    = 'api/locality/create';
   public const URI_LOCALITY_UPDATE                    = 'api/locality/update';
   public const URI_LOCALITY_DELETE                    = 'api/locality/delete';
   public const URI_LOCALITY_GET_BY_ID                 = 'api/locality/find/id';
   public const URI_LOCALITY_GET_BY                    = 'api/locality/find';
   public const URI_LOCALITY_GET_BY_NAME               = 'api/locality/find/name';

    

    /**
     * Undocumented function
     */
    public function __construct(array $conf) {
        parent::__construct($conf);
    }


    /**
     * Undocumented function
     *
     * @return string
     */
    public function getCreateEndpoint() {
        return self::URI_LOCALITY_CREATE;
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getUpdateEndpoint(int $id) {
        return sprintf(self::URI_LOCALITY_UPDATE."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getDeleteEndpoint(int $id) {
        return sprintf(self::URI_LOCALITY_DELETE."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getGetByIdEndpoint(int $id) {
        return sprintf(self::URI_LOCALITY_GET_BY_ID."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param string $name
     * 
     * @return string
     */
    public function getGetByNameEndpoint(string $name) {
        return sprintf(self::URI_LOCALITY_GET_BY_NAME."/%s", $name);
    }

    /**
     * Undocumented function
     * 
     * @return string
     */
    public function getGetByEndpoint() {
        return (self::URI_LOCALITY_GET_BY);
    }
}
