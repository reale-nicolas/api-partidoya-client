<?php
namespace RealeNicolas\ApiPartidoyaClient\Exceptions;

use Exception;

class ValidationException extends Exception {

    protected $status;
    protected $errors = [];


    public function __construct($status, $errors) {

        parent::__construct('There were validation errrors', 422);
        
        $this->setStatus($status);
        $this->setErrors($errors);
    }

    /**
     * Get the value of errors
     */ 
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set the value of errors
     *
     * @return  self
     */ 
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}