<?php
namespace RealeNicolas\ApiPartidoyaClient\Exceptions;

use Exception;

class InvalidClientException extends Exception {

   
    public function __construct() {

        parent::__construct('There were a problem trying to connect with the service', 403);
        
    }
}