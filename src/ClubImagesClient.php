<?php
namespace RealeNicolas\ApiPartidoyaClient;

use Exception;
use RealeNicolas\ApiPartidoyaClient\Contracts\ClubImagesInterface;

/**
 * 
 */
Class ClubImagesClient extends ApiPartidoYaClient implements ClubImagesInterface{

    //Images
    public const URI_CLUB_GET_IMAGE                 = 'api/club/images';
    public const URI_CLUB_CREATE_IMAGE              = 'api/club/images';


    /**
     * Undocumented function
     */
    public function __construct(array $conf) {
        parent::__construct($conf);
    }


    /**
     * Undocumented function
     *
     * @return string
     */
    public function getCreateEndpoint() {
        return sprintf(self::URI_CLUB_CREATE_IMAGE);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getUpdateEndpoint(int $id) {
        throw new Exception("metodo no implementado");
        //return sprintf(self::URI_CLUB_UPDATE."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getDeleteEndpoint(int $id) {
        throw new Exception("metodo no implementado");
        //return sprintf(self::URI_CLUB_DELETE."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * 
     * @return string
     */
    public function getGetByIdEndpoint(int $id) {
        return sprintf(self::URI_CLUB_GET_IMAGE."/%d", $id);
    }

    /**
     * Undocumented function
     *
     * @param string $name
     * 
     * @return string
     */
    public function getGetByNameEndpoint(string $name) {
        throw new Exception("metodo no implementado");
    }


    /**
     * Undocumented function
     * 
     * @return string
     */
    public function getGetByEndpoint() {
        throw new Exception("metodo no implementado");
    }
}
